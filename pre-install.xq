xquery version "3.1";

(:~ will create the system/config collection for this application and stores
 :  the *.xconf from the root of this application there.
 :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

declare function local:mkcol-recursive($collection, $components) {
    if (exists($components)) then
        let $newColl := concat($collection, "/", $components[1])
        return (
            xmldb:create-collection($collection, $components[1]),
            local:mkcol-recursive($newColl, subsequence($components, 2))
        )
    else
        ()
};

(: Helper function to recursively create a collection hierarchy. :)
declare function local:mkcol($collection, $path) {
    local:mkcol-recursive($collection, tokenize($path, "/"))
};

(: store the collection configuration :)
(:for $collection in ( $target, $target || "/textgrid/data", $target || "/textgrid/meta", $target || "/textgrid/rdf" ) :)
(:return:)
  (
  local:mkcol("/db/system/config", $target),   local:mkcol("/db/system/config", $target||"/textgrid/data"),
  xmldb:store-files-from-pattern(concat("/db/system/config", $target), $dir, "**/collection.xconf", "text/xml", true())
)
