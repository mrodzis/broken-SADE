xquery version "3.1";

declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace xconf="http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: the target collection into which the app is deployed :)
declare variable $target external;

let $project-name := tokenize($target, "/")[last()]
let $log := util:log-system-out("installing " || $project-name)

let $system-path := system:get-exist-home() || util:system-property("file.separator")
let $use-template := xmldb:rename($target || "/textgrid/data", "collection-template.xconf", "collection.xconf")
let $data-xconf := $target || "/textgrid/data/collection.xconf"

(: prepare empty Lucene Config files :)
let $prepare-config := (
      util:eval("file:serialize-binary( xs:base64Binary(util:base64-encode(""# empty config file&#10;"")), $system-path || $project-name || '-charmap.txt')"),
      util:eval("file:serialize-binary( xs:base64Binary(util:base64-encode(""# empty config file&#10;"")), $system-path || $project-name || '-synonyms.txt')")
      )

(: rewrite the local path in index config  :)
let $rewrite-mappings := update value
                doc( $data-xconf )
                  //xconf:lucene/xconf:analyzer/xconf:param[@name="mappings"]
                  /@value
                with $system-path || $project-name || "-charmap.txt"
let $rewrite-synonyms := update value
                doc( $data-xconf )
                  //xconf:lucene/xconf:analyzer/xconf:param[@name="synonyms"]
                  /@value
                with $system-path || $project-name || "-synonyms.txt"
let $store-xconf := xmldb:store("/db/system/config"||$target||"/textgrid/data", "collection.xconf", doc( $data-xconf ))
let $reindex := xmldb:reindex($target||"/textgrid/data")
(: we have to test for writable path :)
let $path :=  $system-path
let $user := util:system-property("user.name")
let $message1 := $path || " is not available. Create it and make sure "||$user||" can write there."
let $message2 := "Could not write to " || $path || "."
return (
if
    (file:is-directory($path))
then
    try { file:serialize-binary( xs:base64Binary(util:base64-encode("beacon")) , $path || "beacon.txt") }
    catch * { util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message2 || " &#27;[0m" ) }
else
    util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message1 || " &#27;[0m" )
,

(: run tests on GitLab Runner or when forced :)
let $jobId := try {file:read("/tmp/ci.job") => xs:int()} catch * { 0 }
let $log := util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    "This is JOB #" || string($jobId) || "." || " &#27;[0m" )
return
(
  if ($jobId gt 0)
  then
      (
          let $tests := util:eval(xs:anyURI('test.xq'))
          let $print := util:log-system-out( $tests )
          let $file-name := system:get-exist-home()||util:system-property('file.separator')||".."||util:system-property('file.separator')||"sade_job-"||string($jobId)||".log.xml"
          let $file := file:serialize(<tests time="{current-dateTime()}">{ $tests }</tests>, $file-name, ())
          return
              (   util:log-system-out("Hey! I am taken captive in this Docker! Let me out!"),
                  util:log-system-out("They forced me to say: "||$file || " for " || $file-name),
                  system:shutdown(15)
              )
      )
  else
      util:log-system-out("CI_JOB_ID: 0 (not found)")
  ),
  (: send a beacon to tell the engine, that installation is nearly complete –
   e.g. CI can initiate shutdown procedure afterwards. :)
  util:eval("file:serialize(<done/>, $system-path || 'sade.beacon', ())")
)
