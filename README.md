# SADE
Scalable Architecture for Digital Editions

This is the main application for SADE. It serves as XAR package according to the
[EXPath Packaging System](http://expath.org/spec/pkg) and is created for the
[eXist database](https://exist-db.org)

## The git repo
This repo brings you the sources of the application which contain a brief
documentation as long as you are not dealing with a fork. Please find the
documentation in the folder `docs` where they are stored in Markdown. Any
default installation of this package serves the documentation as website
integrated in your template and in your instance.

### git hooks
The folder `.hooks` contains git hook scripts to be executed on certain git
events. The usage of these scripts is recommended, as they ensure a smooth
workflow.
#### pre-commit
We test for `DONOTCOMMIT` comments in the code. As long as a comment like this
is in files, no commit is possible.
#### post-commit
On any commit a new artifact is being created, so you can proceed and test it
immediately.

## Build
`ant`

Artifacts go to `build/`.

The default artifact is the xar package.

## Deploy
## Installation

## Known Issues
In the unlikely event of cycling to the next digit in TextGrid URIs you may
encounter issues when you going to use an older URI together with a newer that
starts with the same characters. Example: One will publish the text
`textgrid:foo.0` and over time the URI number increases that much, that
another object that should be stored in the database has the URI
`textgrid:foobar.0`, some processes that tests against using `starts-with()`
will fail or may respond with the wrong object. At the end of 2017 we are
dealing with 5 digits of Latin letters and Arabic numbers (a total of 36
different symbols) starting with "3". So it will take another approx. ten
million items to reach the next digit. So we will talk about this later.
