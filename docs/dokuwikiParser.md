# DokuwikiParser
To be able to maintain the pages containing content about project, documentation
and other stuff an comfortable editor and a system that stores the history of a
document is very helpful. In many cases projects using wikis for communication,
coordination and as a general knowledge base. By adding selected pages to a
white-list, the wiki may be become the place for editing the textual content of
the SADE webpage.

The dokuwikiParser was developed by the project on Fontane-Notizbücher.
All the code is in `modules/wiki/wiki.xqm`.
