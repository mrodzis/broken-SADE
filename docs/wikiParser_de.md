# Inhalte aus einem Wiki laden/parsen

Dieses Modul kann Seiten eines Wikis parsen, um sie als HTML Seiten darszustellen.
Es sind zwei verschiedene Parser verfügbar:
* Confluence Wiki (mit dem Modul für **Mehrsprachigkeit kompatibel**!)
* DokuWiki (**NICHT** mit dem Modul für **Mehrsprachigkeit kompatibel**!)

Beide Parser würden auch parallel funktionieren, unterscheiden sich aber in ihrer Funktionsweise, der Konfiguration und den Vorausetzungen.

## Dokumentation
Bitte lesen sie vor der Benutzung die jeweilige Dokumentation:
* **[Conluence Wiki](confluenceParser.md)**
* **[DokuWiki](dokuwikiParser.md)**
