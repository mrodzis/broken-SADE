# Faceted-Search

relations to other documents:
+  [index configuration](index-configuration.md)


## Configuration – Base
As always the configuration is stored at `config.xml` in the applications root
collection. Please look for `<module key="faceted-search">`.
Besides the facets there are several options and endpoints to configure:

|key|configuration|
|--|--|
|hits-per-page|hits pagination|
|kwic-hits|KWIC hits to show per document|
|kwic-width|how many left/right neighbors to show for every hit|
|query-root|XML node the query is send to; **always** use one that is specified in your data collections `collection.xconf` (and so available for Lucene)|
|result-title|title for a document hit|
|result-xslt|XSLT to apply to the hits|
|thumbnail|!LEGACY show/hide thumbnails for a document|
|viewer-html|the link that every result points to|

## Configuration – Facets
To set up own facets you have to provide a `@key` (for internal usage and
debugging), a `@title` (used as headline) and a corresponding XPath that is
applied to all hits on a `descendant-or-self` axis.
When a namespace is needed that is not specified in `modules/faceted-search.xqm`,
there are three possible options:
+ specify namespace there
+ use a wildcard namespace `*:` (untested)
+ a valid explicit QName notation may be added here (untested).

```xml
<param key="facets">
    <facet key="authors" title="Authors">
        <xpath>tei:author</xpath>
    </facet>
    <facet key="keywords" title="Schlagwort">
        <xpath>tei:term</xpath>
    </facet>
    <facet key="dates" title="Zeit">
        <xpath>tei:date/@when</xpath>
    </facet>
</param>
```
