# Mehrsprachigkeit

Dieses Modul ermöglicht mehrsprachige Inhalte. Standardmäßig ist Deutsch als Defaultsprachig eingestellt und Englisch als Alternativsprache.

## Konfiguration des Moduls
Die Konfiguration für dieses Modul befindet sich in der `config.xml`.
Dort muss unter dem Modul `multilanguage` im Parameter `lang.default` eine Defaultsprache (bzw. das entsprechende Sprachenkürzel) angegeben werden. Unter dem Parameter `lang.alt` können beliebig viele weitere Sprachen hinzugefügt werden. Diese müssen mit einem Semikolon getrennt werden. Im folgenden Beispiel sind die Sprachen Deutsch, Englisch und Französisch konfiguriert:
```xml
<module key="multilanguage">
  <param key="lang.default">de</param>
  <param key="lang.alt" description="a semicolon seperated list of alternative languages">en;fr</param>
</module>
```
Bei Abweichungen von der Standardkonfiguration muss der Languageswitcher bearbeitet werden. Standardmäßig ist dieser in der `page_index.html` vorhanden. Dort muss für jede Sprache ein a-Element mit dem dem data-template-to Parameter für die zuvor konfigurierten Sprachenkürzel angelegt werden.
```html
<a data-template="app:switchLanguage" data-template-to="de">Deutsch</a>
<a data-template="app:switchLanguage" data-template-to="en">English</a>
```

## Benutzung
In der Navigationsleiste `navigation.xml` müssen für jede zusätzlich zu der Defaultsprache konfigurierten Sprache ein Label erstellt werden. Beispiel für Deutsch als Defaultsprache und Englisch als Zweitsprache:
```xml
<submenu label="Weiterführende Links" label-en="Further Links">
```
Um einzelne Wörter, wie Überschriften, auf HTML-Seiten sprachenspezifisch anzeigen zu lassen benutzen sie DIV- oder SPAN-Element mit dem Parameter `data-template="lang:translate"` und einem selbstgewählten `data-template-content` Parameter. Beispiel:
```html
<span data-template="lang:translate" data-template-content="Publications"/>
```
Dieser `data-template-content` Parameter muss einzigartig sein und zusätzlich in der `lang.xml` erstellt werden.
Folgendes Beispiel zeigt die Übersetzungen für das Wort "Search" in Deutsch, Englisch und Französisch:
```xml
<word key="Publications">
  <lang key="de">Publikationen</lang>
  <lang key="en">Publications</lang>
  <lang key="fr">Publications</lang>
</word>
```

## Funktionsweise
Das Modul funktioniert mit dem GET-Parameter "lang". Dieser wird an interne Links automatisch angehängt. Dieses Modul ist kompatibel mit dem Modul Confluence Wiki. Um sprachenspezifische Inhalte aus dem Wiki darzustellen nutzen Sie bitte die Confluence eigenen Makros (z.B. German, English, ...).
