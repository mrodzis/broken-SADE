# Development

## Architecture
![architecture](~assets/diagram.svg)
This is image includes draw.io data.

## Packages
SADE consists of three EXPath packages to run within the eXist database
environment.
- SADE.xar - the main application ([git](https://gitlab.gwdg.de/SADE/SADE) | [built](https://ci.de.dariah.eu/exist-repo/packages.html?package-id=SADE))
- assets.xar - all static resources ([git](https://gitlab.gwdg.de/SADE/assets) | [built](https://ci.de.dariah.eu/exist-repo/packages.html?package-id=sade_assets))
- search engine - to configure the Lucene index ([git](https://gitlab.gwdg.de/fontane-notizbuecher/fontane-lucene-exist-module) | [built](https://ci.de.dariah.eu/exist-repo/public/fontane-lucene-exist-module-2.0.2.xar))

## Dependencies and Requirements
Within eXist the following packages are required:
- Markdown Parser
- Function Documentation
- Shared Resources

Further dependencies may defined by [eXist](http://exist-db.org/exist/apps/doc/quickstart.xml).
Mainly this is Java 8 and sufficient amount of memory 2GiB should be available
for exclusive usage (`Xmx`).

## Build
There are `ant` tasks available in a separate [git repo](https://gitlab.gwdg.de/SADE/build)
. This build script generate a complete package containing the eXist database and all required components. Configure this build via the `generic.build.properties` file.

Every package can be built separate by calling `ant` in the root directory. The
result will be a `build\*.xar` file.

## Physical Structure
The file structure is pretty usual for EXPath application within eXist.
To read more on this, the chapter about the Model View Controller in the eXist
book is highly recommended.

All basic files are stored in the root directory, especially the main configuration
and the controller.xql (the main router in the MVC model), index configuration
and package descriptions.

```
   root
     |-- docs
     |-- modules
     |-- templates
     |-- textgrid
```

## Request Processing by Example
All requests for HTML pages are handled by the controller.xql
