# Fork
When working with a SADE instance users usually start manipulating the output,
preparing own transformations or want to use a different configuration for the
lucene index. These changes should be tracked in a Version Control System like
*git* and also they should be deployed to a standalone application that does not
interfere with others. That's why SADE offers a mechanism to fork any instance at
any state. Developers can use this to continue development and to new users it
offers a playground for testing purposes.
At the SADE Publisher within the Lab enter a new project name at the input field
in the first panel (1).
[SCREENSHOT]

Click on "create" (2) and wait until you get redirected to your own instance.
[Enter a new secure password!]
To continue you have to enter new credentials to the SADE Plugin configuration.

> URL: append -ProjectName to "/sade
>
> User: ProjectName
>
> Password:

The username will be the same as the project name and you have to enter the
formerly typed password here as well.

# Download & Tracking changes
You can download any application by clicking on the download button at the SADE
Publihser GUI. This gives you a XAR package according to the EXPath package
standard which is in fact a ZIP archive.
When any further development is planned, fork the offical SADE repo from
[https://gitlab.gwdg.de/SADE/SADE](https://gitlab.gwdg.de/SADE/SADE) clone the
repo to your local machine and replace the files with those from the XAR archive.
This is for complete upstream compatibility.

When you want to publish your fork in the SADE group (for example to make it
part of the [reference instance](https://sade.textgrid.de)) you have to rename
the repository.
