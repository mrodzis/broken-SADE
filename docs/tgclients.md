# TextGrid Clients

There are XQuery functions to talk to the REST and SOAP interfaces of TextGrid
available in `/modules/textgrid/`. They are mainly used to READ data from any
(public|private) part of the repository – see [publish.md](publish.md) –,
but can be used to do any other operation, eg. manipulating data, creating data
and more. A possible use case may is the
logging of information in a file available in TextGrid - so it will be
independent from you installation and will survive an `apt purge` on the server.
You can use SADE to cleanup data, rewrite files, put some information about
validation to the project you and your team is working on.

For description of the available functions, see the XQdocs annotations or have
a look at the visualization on any running server at [`fundocs.html`](http://localhost:8080/exist/apps/sade/fundocs.html).
