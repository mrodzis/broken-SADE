# Customization
SADE was build to fit the needs of many projects creating digital editions.
For a complete customization, a adaption of some XQuery modules may becomes
necessary. But a few things are made to set up by any user, as long as she is
familiar with the basics of XML. When you want to start preparing your own
instance, look at the file `config.xml` in the root collection of the application.
There are many things you can set up.

## Top Menu
Edit the top menu (navbar) by entering your preferred structure in the file
`navigation.xml`.

```xml
<navigation>
    <item label="Ein Text" link="view.html?id=text.md"/>
    <submenu label="Links">
        <item label="http://textgrid.de" link="http://textgrid.de"/>
    </submenu>
</navigation>
```
