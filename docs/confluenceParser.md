# Confluence-Wiki-Parser

Dieses Teilmodul kann **öffentliche** Seiten eines Confluence Wikis parsen, um sie als HTML Seiten darszustellen.
Es ist mit dem Modul für **Mehrsprachigkeit kompatibel**!

## Konfiguration des Moduls
Die Konfiguration für dieses Modul befindet sich in der `config.xml`. Dort muss lediglich die (Haupt-)URL der REST Schnittstelle des Wikis angegeben werden.
```xml
<param key="confluence.url">https://wiki.de.dariah.eu/rest/api/content/</param>
```

## Allgemeine Benutzung
Nach der Konfiguration kann auf der gewünschten HTML Seite der Inhalt einer Wikiseite folgendermaßen eingebunden werden:
```html
<div data-template="confluence:confluence" data-template-id="44769550"/>
```
Das Attribute `data-template-id` muss durch die pageID der Wikiseite ersetzt werden.

Standardmäßig ist die Datei wiki.html im template vorhanden. Dieser Seite kann die pageID als Parameter übergeben werden. So können Wikiinhalte mit einem gewünschten Templates umrundet werden.
Beispiel: `doku.html?id=44769550`

## Benutzung mit dem Modul Mehrsprachigkeit
Durch das Konfigurieren des Moduls für Mehrsprachigkeit sind die Sprachen über Sprachenkürzel in der `config.xml` angegeben. Fügen sie bitte für jedes dieser Sprachenkürzel (auch das der Defaultsprache) ein Mapping im `<module key="wiki">` mit dem Confluence eigenen Makronamen der Sprache hinzu. Im folgenden Beispiel sind die Sprachen Deutsch, Englisch und Französisch konfiguriert:
```xml
<param key="confluence.lang" description="if the multilanguage plugin is enabled, used languages specified here">
  <lang code="de" name="german"/>
  <lang code="en" name="english"/>
  <lang code="fr" name="french"/>
</param>
```

## Funktionsweise
Für die jeweilige Wikiseite wird beim Aufruf des Nutzers geprüft, ob es eine neue Revision gibt. Ist dies der Fall, wird die neue Revision geparst und in der Datenbank gespeichert. Ist das Modul für Mehrsprachigkeit aktiviert, wird für jede konfigurierte Sprache je eine Datei gespeichert.
