xquery version "3.1";

import module namespace dbutil="http://exist-db.org/xquery/dbutil";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

declare function local:download($app-collection as xs:string, $expathConf as element(), $expand-xincludes as xs:boolean) {
    let $name := concat($expathConf/@abbrev, "-", $expathConf/@version, ".xar")
    let $entries :=
        dbutil:scan(xs:anyURI($app-collection), function($collection as xs:anyURI?, $resource as xs:anyURI?) {
            let $resource-relative-path := substring-after($resource, $app-collection || "/")
            let $collection-relative-path := substring-after($collection, $app-collection || "/")
            return
                if (empty($resource)) then
                    (: no need to create a collection entry for the app's root directory :)
                    if ($collection-relative-path eq "") then
                        ()
                    else
                        <entry type="collection" name="{$collection-relative-path}"/>
                else if (util:binary-doc-available($resource)) then
                    <entry type="uri" name="{ $resource-relative-path }">{ $resource }</entry>
                else
                    <entry type="xml" name="{ $resource-relative-path }">{
                        util:declare-option("exist:serialize", "expand-xincludes=" || (if ($expand-xincludes) then "yes" else "no")),
                        doc($resource)
                    }</entry>
        })
    let $xar := compression:zip($entries, true())
    return (
        response:set-header("Content-Disposition", concat("attachment; filename=", $name)),
        response:stream-binary($xar, "application/zip", $name)
    )
};

let $collection := $config:app-root
let $expathConf := xmldb:xcollection($collection)/*:package
let $expand-xincludes := request:get-parameter("expand-xincludes", "false") cast as xs:boolean

return
local:download($collection, $expathConf, $expand-xincludes)
