xquery version "3.1";
(:~
 : This is the main viewer module providing functions to visualize all kinds of
 : documents, e.g. TEI, Markdown or HTML passthrou.
 : @author Mathias Göbel
 : @author Ubbo Veentjer
 : @version 1.0
:)
module namespace multiviewer="https://sade.textgrid.de/ns/multiviewer";

declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace templates="http://exist-db.org/xquery/templates";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace xlink="http://www.w3.org/1999/xlink";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace markdown="http://exist-db.org/xquery/markdown";
import module namespace dsk-view="http://semtonotes.github.io/SemToNotes/dsk-view"  at 'semtonotes/SemToNotes.xqm';

declare function multiviewer:show(
    $node as node(),
    $model as map(*),
    $id as xs:string,
    $view as xs:string?) {

let $id :=  multiviewer:idResolver($id)
let $docpath := multiviewer:getPath($id)

return
    if($id = "") then error( QName("https://sade.textgrid.de/ns/multiviewer", "SHOW01"), "The provided URI is or leads to empty.") else
    switch(tokenize($docpath, "\.")[last()])
        case "xml" return
            (: check for TILE :)
            (: process TEI :)
                let $doc := doc( $docpath )
                    return
                    if( $doc/*/namespace-uri() = "http://www.tei-c.org/ns/1.0" )
                    then multiviewer:TEI( $doc/tei:TEI )
                    else error(QName("https://sade.textgrid.de/ns/multiviewer", "SHOW03"), "The provided document is in an unsupported namespace. Give me TEI!")
        case "md" return multiviewer:markdown($docpath)
        default return
            error( QName("https://sade.textgrid.de/ns/multiviewer", "SHOW02"), "Sorry, there is nothing I can do for you.")
};

(:~
 : Provide any document as is, serialized to plain text.
 :
 :  :)
declare
%templates:wrap
function multiviewer:raw($node as node(), $model as map(*), $id) {
    let $id := multiviewer:idResolver($id)
    let $path := multiviewer:getPath($id)
    let $isBinary as xs:boolean := util:is-binary-doc($path)
    let $doc := if( $isBinary )
                then util:binary-doc( $path ) => util:base64-decode()
                else doc( $path )
    return
        if($isBinary)
        then
            (attribute class {"markdown"},
            serialize( $doc, map{"method":"text", "indent":true()} ))
        else (attribute class {"xml"},
                serialize( $doc, map{"method":"xml", "indent":true()} ))
};

(:~
 : A helper function providing the complete path to a given short path.
 : It is useful when data and documentation is seperated in different collections.
 : @param $id – the id used in the frontend to point to the document
 :   :)
declare function multiviewer:getPath($id as xs:string) as xs:string {
    if( starts-with($id, "docs/") )
    then $config:app-root || "/" || $id
    else if (starts-with($id, "/docs/"))
    then $config:app-root || $id
    else ($config:data-root || '/' || $id) => replace("//", "/")
};

(:~
 : A helper function to resolve textgrid:IDs
 : @param $id – the id used in the frontend to point to the document
 :   :)
declare function multiviewer:idResolver($id as xs:string) as xs:string {
let $textgridUris := collection( $config:app-root || "/textgrid/meta" )//tgmd:textgridUri
return
if(starts-with($id, "/") or starts-with($id, "docs/"))
    then
        $id
else if(starts-with($id, "textgrid:"))
    then
        let $format := $textgridUris[starts-with(.,$id)]/ancestor::tgmd:generic/tgmd:provided/tgmd:format
        let $base-uri := $format/base-uri() => substring-after( $config:data-root )
        return
            switch ($format)
                case "text/xml" return replace($base-uri, "/meta/", "/data/")
                case "text/linkeditorlinkedfile" return replace($base-uri, "/meta/", "/tile/")
                default return error( QName("https://sade.textgrid.de/ns/multiviewer", "SHOW04"), "Can not parse "||$format||".")
    else if($textgridUris[contains(., $id)])
    then
        let $format := $textgridUris[contains(., $id)]/ancestor::tgmd:generic/tgmd:provided/tgmd:format
        let $base-uri := $format/base-uri() => substring-after( $config:data-root )
        return
            switch ($format)
                case "text/xml" return replace($base-uri, "/meta/", "/data/")
                case "text/linkeditorlinkedfile" return replace($base-uri, "/meta/", "/tile/")
                default return error( QName("https://sade.textgrid.de/ns/multiviewer", "SHOW04"), "Can not parse "||$format||".")
    else
        error( QName("https://sade.textgrid.de/ns/multiviewer", "SHOW05"), "Can not parse "||$id||". Expecting a string starting with either ""textgrid:"" or ""/"" (path).")
};

declare function multiviewer:markdown($docpath as xs:string) as item()* {

    let $inputDoc := util:binary-doc( $docpath )
    let $input := util:binary-to-string($inputDoc)
    let $markdown := markdown:parse($input)

    return
        <xhtml:div class="markdown">
            {local:markdownTransform($markdown)}
        </xhtml:div>
};

declare function multiviewer:TEI($TEI as element(tei:TEI)) as node()* {
    let $stylesheet := doc( substring-before($config:app-root, "/sade") || "/sade_assets/TEI-Stylesheets/html5/html5.xsl")
    let $transform := transform:transform($TEI, $stylesheet, ())/xhtml:body/node()

return
    <div class="teixslt">
        {$transform}
    </div>
};

declare function local:markdownTransform($nodes) {
for $node in $nodes
return
    if( $node/local-name() = "h1" ) then
        (: the typeswtich did not cast h1. :)
            <xhtml:h2 class="block-header">
                <xhtml:span class="title">{ $node/node() }</xhtml:span>
            </xhtml:h2>
    else
    typeswitch ( $node )
        case element( body ) return
            local:markdownTransform($node/node())
        case text() return $node
        default return
          element { xs:QName("xhtml:" || $node/local-name()) }
                  {   $node/@*,
                      local:markdownTransform( $node/node() )
                  }
};



declare function multiviewer:renderTILE($node as node(), $model as map(*), $docpath as xs:string) as item()* {
let $doc := doc($docpath)
let $i := $doc//tei:link[starts-with(@targets, '#shape')][1]
let $shape := substring-before(substring-after($i/@targets, '#'), ' ')
let $teiuri :=  if(contains(substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a'), '.'))
                                then substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a')
                                else
                                    (: todo: find lates revision in collection :)
                                    substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a') || '.0'
let $imageuri := $doc//svg:image[following::svg:rect/@id eq $shape]/string(@xlink:href)
let $imgwidth := $doc//svg:image/@width/number()

let $teidoc := doc(substring-before($docpath, 'tile') || 'data/' || $teiuri || '.xml')/*
let $html := dsk-view:render($teidoc, $imageuri, $imgwidth, $docpath)//xhtml:body/*

return <div id="stn">
   {$html}
    </div>
};

(: TODO: tei-specific :)
declare function multiviewer:tei-paging($doc, $page as xs:integer) {
    let $doc := if ($page > 0 and ($doc//tei:pb)[$page]) then
            util:parse(util:get-fragment-between(($doc//tei:pb)[$page], ($doc//tei:pb)[$page+1], true(), true()))
            (: Kann das funktionieren, wenn page als integer übergeben wird? müsste man nicht tei:pb/@n auswerten? :)
        else
            $doc
        return $doc
};

declare function multiviewer:LEGACY-choose-xsl-and-transform($doc, $model as map(*)) {
    let $namespace := namespace-uri($doc/*[1])
    let $xslconf := $model("config")//module[@key="multiviewer"]//stylesheet[@namespace=$namespace][1]

    let $xslloc := if ($xslconf/@local = "true") then
            config:get("project-dir")|| "/" ||  $xslconf/@location
        else
            $xslconf/@location
    let $xsl := doc('/db/sade-projects/textgrid/data/xml/data/1vzvf.8.xml')
    let $html := transform:transform($doc, $xsl, $xslconf/parameters)

    return $html
};
