xquery version "3.1";

module namespace lang="https://sade.textgrid.de/ns/lang";

import module namespace app="https://sade.textgrid.de/ns/app" at "app.xql";
import module namespace functx="http://www.functx.com";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

declare namespace lf="https://sade.textgrid.de/ns/langfile";

declare variable $lang:lang := app:getLanguage();

declare function lang:translate( $node as node(), $model as map(*), $content as xs:string ) as node() {

let $langconfig := doc( $config:app-root || "/lang.xml" )
return
    typeswitch($node)
        case element (span) return element {name($node)} {
            $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]
        }
        case element (input) return element input {
            (: Language stuff for search in results.html :)
            if ($content = "SearchResults") then (
                attribute id {"searchLang"},
                attribute type {"hidden"},
                attribute name {"lang"},
                attribute value {$lang:lang}
            )
            (: Placeholder stuff for input items :)
            else (
                attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                    else ()
            )
        }
        case element (textarea) return element textarea {
            attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else ()
        }
        case element (button) return element button {
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else (),
            $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]
        }
        default return element {name($node)} {}
};
