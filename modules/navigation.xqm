xquery version "3.1";
module namespace nav="https://sade.textgrid.de/ns/navigation";

import module namespace app="https://sade.textgrid.de/ns/app" at "app.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace templates="http://exist-db.org/xquery/templates";

declare variable $nav:module-key := "navigation";
declare variable $nav:lang := app:getLanguage();
declare variable $nav:langDefault := (config:get("lang.default", "multilanguage"));
declare variable $nav:langTest :=   if($nav:langDefault = "")
                                    then true()
                                    else ($nav:lang = $nav:langDefault);

declare
    %templates:wrap
function nav:navitems($node as node(), $model as map(*)) as map(*) {
    let $confLocation := config:get("location", $nav:module-key)
    let $navitems := doc( $config:app-root || "/" || $confLocation)//navigation/*
    return
        map { "navitems" := $navitems }
};

declare function nav:head($node as node(), $model as map(*)) {
    switch(local-name($model("item")))
        case "submenu" return
            element { node-name($node) } {
                    $node/@*,
                    if ($nav:langTest)
                    then (string($model("item")/@label))
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang]),
                    $node/node()
            }
        case "item" return
            element { node-name($node) } {
                    attribute href {$model("item")/@link},
                    if ($nav:langTest or not($model("subitem")/@*[local-name() ="label-" || $nav:lang]))
                    then string($model("item")/@label)
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang])
            }
        default return
            <b>not defined: { node-name($model("item")) }</b>
};

declare
    %templates:wrap
function nav:subitems($node as node(), $model as map(*)) as map(*) {
    map{ "subitems" := $model("item")/*}
};

declare function nav:subitem($node as node(), $model as map(*)) {
    if($model("subitem")/@class) then
        <span class="{$model("subitem")/@class}">&#160;</span>
    else if ($model("subitem")/name() != 'divider') then
        element a {
            if(string($model("subitem")/@link)) then attribute href { string($model("subitem")/@link)} else (),
            if ($nav:langTest or not($model("subitem")/@*[local-name() ="label-" || $nav:lang]))
            then string($model("subitem")/@label)
            else string($model("subitem")/@*[local-name( . ) = "label-" || $nav:lang])
        }
        else <span>&#160;</span>
};
