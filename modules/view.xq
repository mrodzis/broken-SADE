xquery version "3.1";
(:~
 : This is the main XQuery which will (by default) be called by controller.xql
 : to process any URI ending with ".html". It receives the HTML from
 : the controller and passes it to the templating system.
 :)

(:
 : The following modules provide functions which will be called by the
 : templating. Register new modules here.
:)
import module namespace app="https://sade.textgrid.de/ns/app" at "app.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace confluence="https://sade.textgrid.de/ns/wiki-confluence" at "wiki/confluence.xqm";
import module namespace lang="https://sade.textgrid.de/ns/lang" at "lang.xqm";
import module namespace fsearch="https://sade.textgrid.de/ns/faceted-search" at "faceted-search.xqm";
import module namespace multiviewer="https://sade.textgrid.de/ns/multiviewer" at "multiviewer.xqm";
import module namespace nav="https://sade.textgrid.de/ns/navigation" at "navigation.xqm";
import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace tgmenu="https://sade.textgrid.de/ns/tgmenu" at "textgrid/menu.xqm";

import module namespace functx="http://www.functx.com";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace xhtml="http://www.w3.org/1999/xhtml";


declare option output:method "html5";
declare option output:media-type "text/html";

declare function local:linkrewrite($nodes as node()*) as node()* {
for $node in $nodes
return
    typeswitch ( $node )
    case element( a ) return
        if(starts-with($node/@href, "http") or starts-with($node/@href, "#"))
        then
            element xhtml:a {
                    $node/@*,
                    local:linkrewrite($node/node())
            }
        else
            element xhtml:a { $node/@*[local-name(.) != "href"],
                attribute href { app:rewriteLink(string($node/@href)) },
                local:linkrewrite($node/node())
            }
    case element( form ) return
        element xhtml:form {
            $node/@*,
            element xhtml:input {
                attribute type {"hidden"},
                attribute name {"lang"},
                attribute value {app:getLanguage()}
            },
            local:linkrewrite($node/node())
        }
    case text() return $node
    case comment() return $node
    case processing-instruction() return $node
    case element() return
        element {QName("http://www.w3.org/1999/xhtml", local-name($node))} {
            $node/@*,
            local:linkrewrite($node/node())
        }
    default return local:linkrewrite($node/node())
};


let $config := map {
    $templates:CONFIG_APP_ROOT : $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR : true()
}
(:
 : We have to provide a lookup function to templates:apply to help it
 : find functions in the imported application modules. The templates
 : module cannot see the application modules, but the inline function
 : below does see them.
 :)
let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}
(:
 : The HTML is passed in the request from the controller.
 : Run it through the templating system and return the result.
 :)
let $content := request:get-data()
let $page := templates:apply($content, $lookup, (), $config)

return
    if(config:key-available("multilanguage"))
    then local:linkrewrite($page)
    else $page
